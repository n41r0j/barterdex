.. BarterDEX documentation master file, created by
   sphinx-quickstart on Tue Jan 16 16:12:09 2018.

Welcome to BarterDEX's documentation!
=====================================

BarterDEX is a decentralized cryptocurrency exchange using atomic swaps to trade coin A directly with coin B, without needing to trust a third party.

Todo

- Tier Nolan protocol
- Decentralized orderbooks
- Decentralized ordermatching

* :ref:`user-docs`
* :ref:`developer`

.. _user-docs:

.. toctree::
   :maxdepth: 2
   :caption: User documentation

   faq
   getting_started
   guides

.. _developer:

.. toctree::
   :maxdepth: 1
   :caption: Developer

   api
   whitepaper
